package ru.android.mike.algorithmbook.mvp.presenter;

import ru.android.mike.algorithmbook.mvp.view.View;

public interface Presenter<T extends View> {

    void attachView(T view);
    void detachView();

}
