package ru.android.mike.algorithmbook;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.android.mike.algorithmbook.adapter.NoteAdapter;
import ru.android.mike.algorithmbook.di.App;
import ru.android.mike.algorithmbook.model.Note;
import ru.android.mike.algorithmbook.mvp.presenter.NotePresenter;
import ru.android.mike.algorithmbook.mvp.view.NoteView;

public class NoteActivity extends BaseActivity implements NoteView, NoteAdapter.OnCardClickListener {

    @BindView(R.id.note_recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.add_note_fab)
    FloatingActionButton mAddNoteFab;

    @Inject
    NotePresenter mPresenter;

    private NoteAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        ButterKnife.bind(this);

        App.getComponent().injectNoteActivity(this);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new NoteAdapter();
        mAdapter.setClickListener(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onPause() {
        mPresenter.detachView();
        super.onPause();
    }

    @Override
    protected void onResume() {
        mPresenter.attachView(this);
        mPresenter.loadNotes();
        super.onResume();
    }

    @OnClick(R.id.add_note_fab)
    public void onViewClicked() {
        startActivityForResult(new Intent(this, AddNoteActivity.class).setFlags(1), 1);
    }

    @Override
    public void updateUi(List<Note> notes) {
        mAdapter.setNotes(notes);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void updateAdapter(int position) {
        mAdapter.notifyItemRemoved(position);
    }

    @Override
    public void deleteNote(int id, int position) {
        mPresenter.deleteNote(id, position);
    }

    @Override
    public void editNote(Note note, int position) {
        Intent intent = new Intent(this, AddNoteActivity.class).setFlags(0);
        intent.putExtra(AddNoteActivity.NOTE_ID, note.getId());
        intent.putExtra(AddNoteActivity.NOTE_TITLE, note.getTitle());
        intent.putExtra(AddNoteActivity.NOTE_DESCRIPTION, note.getDescription());
        startActivityForResult(intent, 2);
    }
}
