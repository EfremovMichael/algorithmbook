package ru.android.mike.algorithmbook.di.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.android.mike.algorithmbook.strorage.Database;

@Module
public class StorageModule {

    private Context mContext;

    public StorageModule(Context context) {
        mContext = context;
    }

    @Provides
    @Singleton
    Database provideDatabase(){
        return new Database(mContext);
    }
}
