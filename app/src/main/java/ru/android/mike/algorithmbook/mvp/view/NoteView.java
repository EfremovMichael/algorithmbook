package ru.android.mike.algorithmbook.mvp.view;

import java.util.List;

import ru.android.mike.algorithmbook.model.Note;

public interface NoteView extends View {

    void updateUi(List<Note> notes);
    void updateAdapter(int position);

}
