package ru.android.mike.algorithmbook;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    private static final String EXTRA_EMAIL = "extra_email";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    public static Intent newIntent(Context context, String email) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(EXTRA_EMAIL, email);
        return intent;
    }

    @OnClick({R.id.book_card_view, R.id.note_card_view, R.id.test_card_view})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.book_card_view:
                startActivity(new Intent(this, ContentActivity.class));
                break;
            case R.id.note_card_view:
                startActivity(new Intent(this, NoteActivity.class));
                break;
            case R.id.test_card_view:
                startActivity(new Intent(this, TestActivity.class));
                break;
        }
    }
}
