package ru.android.mike.algorithmbook;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public abstract class BaseActivity extends AppCompatActivity {
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.item_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.return_item:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            case R.id.about_item:
                Toast.makeText(this, R.string.about_app, Toast.LENGTH_SHORT).show();
                break;
            case R.id.exit_item:
                finishAffinity();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
