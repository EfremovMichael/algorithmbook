package ru.android.mike.algorithmbook.strorage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.android.mike.algorithmbook.model.Answer;
import ru.android.mike.algorithmbook.model.Content;
import ru.android.mike.algorithmbook.model.Note;
import ru.android.mike.algorithmbook.model.Question;

public class Database {

    private static final String DATABASE_NAME = "database.db";
    private static final int DATABASE_VERSION = 1;
    private SQLiteDatabase mDatabase;
    private Cursor mCursor;

    public Database(Context context) {
        mDatabase = new DatabaseHelper(context).getWritableDatabase();
    }

//    public void close() {
//        mDatabase.close();
//    }

    public Single<List<Content>> getAllContent(String tableName) {
        return Single.<List<Content>>create(emitter -> {
            mCursor = mDatabase.query(tableName, null, null, null, null, null, null);
            if (mCursor.moveToFirst()) {
                List<Content> contents = new ArrayList<>();
                do {
                    contents.add(new Content(mCursor.getInt(0), mCursor.getString(1), mCursor.getString(2)));
                } while (mCursor.moveToNext());
                emitter.onSuccess(contents);
            } else emitter.onError(new Exception());
            mCursor.close();
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<List<Question>> getQuestions(String tableName) {
        return Single.fromCallable(() -> {
            List<Question> questions = new ArrayList<>();
            List<Answer> answers;
            mCursor = mDatabase.query(tableName, null, null, null, null, null, "random() limit 10");
            if (mCursor.moveToFirst()) {
                int id_index = mCursor.getColumnIndex("_id");
                int title_index = mCursor.getColumnIndex("question_title");
                do {
                    answers = loadAnswers(mCursor.getInt(id_index));
                    questions.add(new Question(mCursor.getInt(id_index), mCursor.getString(title_index), answers));
                } while (mCursor.moveToNext());
            }
            mCursor.close();
            return questions;
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    private List<Answer> loadAnswers(int id) {
        List<Answer> answers = new ArrayList<>();
        Cursor cursor = mDatabase.query("answers", null, "question_id = ?", new String[]{String.valueOf(id)}, null, null, "random() limit 4");
        if (cursor.moveToFirst()) {
            int index_id = cursor.getColumnIndex("_id");
            int index_title = cursor.getColumnIndex("answer");
            int index_istrue = cursor.getColumnIndex("is_true");
            do {
                answers.add(new Answer(
                        cursor.getInt(index_id),
                        cursor.getString(index_title),
                        cursor.getInt(index_istrue)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return answers;
    }

    /**
     * Возвращает список заметок
     *
     * @return Notes
     */
    public Single<List<Note>> loadNotes() {
        return Single.<List<Note>>create(emitter -> {
            mCursor = mDatabase.query("notes", null, null, null, null, null, null, null);
            if (mCursor.moveToNext()) {
                List<Note> notes = new ArrayList<>();
                do {
                    notes.add(new Note(mCursor.getInt(0), mCursor.getString(1), mCursor.getString(2)));
                } while (mCursor.moveToNext());
                mCursor.close();
                emitter.onSuccess(notes);
            } else {
                mCursor.close();
                emitter.onError(new Exception("Error load"));
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Удаляет заметку из бд
     *
     * @param id id заметки
     */
    public Completable deleteNote(int id) {
        return Completable.create(emitter -> {
            mDatabase.delete("notes", "_id = " + id, null);
            emitter.onComplete();
        });
    }

    /**
     * Вставить заметку в бд
     *
     * @param title       Заголовок
     * @param description Описание
     */
    public Completable insertNote(String title, String description) {
        return Completable.create(emitter -> {
            ContentValues cv = new ContentValues();
            cv.put("title", title);
            cv.put("description", description);
            mDatabase.insert("notes", null, cv);
        }).subscribeOn(Schedulers.io());
    }

    public Completable editNote(String title, String description, int id) {
        return Completable.create(emitter -> {
            ContentValues cv = new ContentValues();
            cv.put("title", title);
            cv.put("description", description);
            mDatabase.update("notes", cv, "_id = " + id, null);
            emitter.onComplete();
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    private class DatabaseHelper extends SQLiteAssetHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
    }

}
