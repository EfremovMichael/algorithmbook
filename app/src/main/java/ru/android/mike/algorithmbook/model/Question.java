package ru.android.mike.algorithmbook.model;

import java.util.List;

public class Question {

    private int mId;
    private String mQuestionTitle;
    private List<Answer> mAnswers;

    public Question(int id, String questionTitle, List<Answer> answers) {
        mId = id;
        mQuestionTitle = questionTitle;
        mAnswers = answers;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getQuestionTitle() {
        return mQuestionTitle;
    }

    public void setQuestionTitle(String questionTitle) {
        mQuestionTitle = questionTitle;
    }

    public List<Answer> getAnswers() {
        return mAnswers;
    }

    public void setAnswers(List<Answer> answers) {
        mAnswers = answers;
    }
}
