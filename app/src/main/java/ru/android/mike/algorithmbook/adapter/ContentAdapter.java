package ru.android.mike.algorithmbook.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.android.mike.algorithmbook.R;
import ru.android.mike.algorithmbook.model.Content;

public class ContentAdapter extends RecyclerView.Adapter<ContentAdapter.ViewHolder> {

    public interface ContentClick {
        void contentClick(String path);
    }

    private List<Content> mContents;
    private ContentClick mClick;

    public ContentAdapter(List<Content> contents, ContentClick click) {
        mContents = contents;
        mClick = click;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.content_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(mContents.get(position));
    }

    @Override
    public int getItemCount() {
        return mContents.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title_content_tv)
        TextView mTitle;

        @OnClick(R.id.item_card)
        void onClick() {
            mClick.contentClick(mContents.get(getAdapterPosition()).getDescription());
        }

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Content content) {
            mTitle.setText(content.getName());
        }
    }
}
