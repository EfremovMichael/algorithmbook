package ru.android.mike.algorithmbook.mvp.view;

import com.google.firebase.auth.FirebaseUser;


public interface LoginView extends View {

    void setClicked(boolean b);
    void showToast(String message);
    void showProgressBar();
    void hideProgressBar();
    void goNextActivity(FirebaseUser user);
}
