package ru.android.mike.algorithmbook;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.android.mike.algorithmbook.di.App;
import ru.android.mike.algorithmbook.mvp.presenter.LoginPresenter;
import ru.android.mike.algorithmbook.mvp.view.LoginView;

/**
 * Экран входа в приложение
 * Вход можно выполнить по email и паролю
 * Или зайти без регистрации
 */
public class LoginActivity extends AppCompatActivity implements LoginView {

    private static final String STATE_EMAIL = "state_email";
    private static final String STATE_PASSWORD = "state_password";

    @BindView(R.id.login_progress)
    ProgressBar mProgressBar;
    @BindView(R.id.email)
    AutoCompleteTextView mEmail;
    @BindView(R.id.password)
    EditText mPassword;
    @BindView(R.id.sign_in_button)
    Button mSignInButton;
    @BindView(R.id.create_button)
    Button mCreateButton;
    @BindView(R.id.no_email_sign_in_button)
    Button mNoEmailSignInButton;

    @Inject
    LoginPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        App.getComponent().injectLoginActivity(this);

        if (savedInstanceState != null) {
            mEmail.setText(savedInstanceState.getString(STATE_EMAIL));
            mPassword.setText(savedInstanceState.getString(STATE_PASSWORD));
        }
        mPresenter.attachView(this);
        mPresenter.checkUser();
    }

    @Override
    public void setClicked(boolean b) {
        mCreateButton.setEnabled(b);
        mEmail.setEnabled(b);
        mPassword.setEnabled(b);
        mSignInButton.setEnabled(b);
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
        setClicked(false);
    }

    @Override
    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
        setClicked(true);
    }

    @Override
    public void goNextActivity(FirebaseUser user) {
        if (user != null) {
            startActivity(MainActivity.newIntent(this, user.getEmail()));
            finish();
        }
    }

    @OnClick({R.id.sign_in_button, R.id.create_button, R.id.no_email_sign_in_button})
    public void onViewClicked(View view) {
        String email = mEmail.getText().toString();
        String password = mPassword.getText().toString();
        switch (view.getId()) {
            case R.id.sign_in_button:
                mPresenter.signUser(email, password);
                break;
            case R.id.create_button:
                mPresenter.createUser(email, password);
                break;
            case R.id.no_email_sign_in_button:
                startActivity(MainActivity.newIntent(this, "no enter"));
                finish();
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        String email = mEmail.getText().toString();
        String password = mPassword.getText().toString();
        outState.putString(STATE_EMAIL, email);
        outState.putString(STATE_PASSWORD, password);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        mPresenter.checkInternet(getApplicationContext());
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }
}
