package ru.android.mike.algorithmbook.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.android.mike.algorithmbook.R;
import ru.android.mike.algorithmbook.model.Note;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.ViewHolder> {

    public interface OnCardClickListener {
        void deleteNote(int id, int position);
        void editNote(Note note, int position);
    }

    private List<Note> mNotes;
    private OnCardClickListener mClickListener;


    public void setNotes(List<Note> notes) {
        mNotes = notes;
    }


    public void setClickListener(OnCardClickListener clickListener) {
        mClickListener = clickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_note, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(mNotes.get(position));
    }

    @Override
    public int getItemCount() {
        return mNotes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.note_title)
        TextView mNoteTitle;
        @BindView(R.id.description_note)
        TextView mNoteDescription;

        @OnClick(R.id.ic_trash)
        void onClick() {
            mClickListener.deleteNote(mNotes.get(getAdapterPosition()).getId(), getAdapterPosition());
            mNotes.remove(getLayoutPosition());
        }

        @OnClick(R.id.ic_edit)
        void onEditClick(){
            mClickListener.editNote(mNotes.get(getAdapterPosition()), getAdapterPosition());
        }

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Note note) {
            mNoteTitle.setText(note.getTitle());
            mNoteDescription.setText(note.getDescription());
        }
    }
}
