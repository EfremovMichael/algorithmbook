package ru.android.mike.algorithmbook.di.component;

import javax.inject.Singleton;

import dagger.Component;
import ru.android.mike.algorithmbook.ContentActivity;
import ru.android.mike.algorithmbook.LoginActivity;
import ru.android.mike.algorithmbook.NoteActivity;
import ru.android.mike.algorithmbook.TestActivity;
import ru.android.mike.algorithmbook.di.modules.MvpModule;
import ru.android.mike.algorithmbook.di.modules.StorageModule;
import ru.android.mike.algorithmbook.strorage.Database;

@Component(modules = {MvpModule.class, StorageModule.class})
@Singleton
public interface AppComponent {

    void injectLoginActivity(LoginActivity loginActivity);
    void injectTestActivity(TestActivity testActivity);
    void injectNoteActivity(NoteActivity noteActivity);
    Database getDatabase();
}
