package ru.android.mike.algorithmbook.mvp.view;

import java.util.List;

import ru.android.mike.algorithmbook.model.Question;

public interface TestView extends View {

    void updateUi(List<Question> questions, int countAnswer);
    void showDialog(int correctAnswer, int questionCount);

}
