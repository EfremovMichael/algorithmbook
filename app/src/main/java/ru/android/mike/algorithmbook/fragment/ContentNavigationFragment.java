package ru.android.mike.algorithmbook.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.Disposable;
import ru.android.mike.algorithmbook.R;
import ru.android.mike.algorithmbook.di.App;
import ru.android.mike.algorithmbook.adapter.ContentAdapter;
import ru.android.mike.algorithmbook.model.Content;
import ru.android.mike.algorithmbook.strorage.Database;


public class ContentNavigationFragment extends Fragment implements ContentAdapter.ContentClick {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    Unbinder unbinder;

    private Database mDatabase;
    private Disposable mDisposable;
    private List<Content> mContents;
    private ContentAdapter mAdapter;

    public ContentNavigationFragment() { }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_content_navigation, container, false);
        unbinder = ButterKnife.bind(this, view);

        mDatabase = App.getComponent().getDatabase();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        initList();
        return view;
    }

    public void initList(){
    mDisposable = mDatabase.getAllContent("contents")
            .subscribe(contents -> {
                mContents = contents;
                mAdapter = new ContentAdapter(mContents, this);
                mRecyclerView.setAdapter(mAdapter);
            });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mDisposable.dispose();
        unbinder.unbind();
    }

    @Override
    public void contentClick(String path) {
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, ContentFragment.newInstance(path))
                .addToBackStack("back").commit();
    }
}
