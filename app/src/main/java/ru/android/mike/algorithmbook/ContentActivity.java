package ru.android.mike.algorithmbook;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import java.util.Objects;

import ru.android.mike.algorithmbook.fragment.ContentNavigationFragment;

public class ContentActivity extends BaseActivity {

    private FragmentManager mFragmentManager;
    private Fragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mFragmentManager = getSupportFragmentManager();
        mFragment = mFragmentManager.findFragmentById(R.id.fragment_container);
        if (mFragment == null){
            mFragment = new ContentNavigationFragment();
            mFragmentManager.beginTransaction().replace(R.id.fragment_container, mFragment).commit();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onDestroy() {
        Log.d("myLog", "onDestroy: ");
        super.onDestroy();
    }
}
