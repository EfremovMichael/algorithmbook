package ru.android.mike.algorithmbook.mvp.presenter;

import android.util.Log;

import io.reactivex.disposables.Disposable;
import ru.android.mike.algorithmbook.mvp.view.NoteView;
import ru.android.mike.algorithmbook.strorage.Database;

public class NotePresenter implements Presenter<NoteView> {

    private NoteView mView;
    private Database mDatabase;
    private Disposable mDisposable;

    public NotePresenter(Database database) {
        mDatabase = database;
    }

    @Override
    public void attachView(NoteView view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mDisposable.dispose();
        mView = null;
    }

    public void loadNotes() {
        mDisposable = mDatabase.loadNotes().subscribe(notes -> {
                mView.updateUi(notes);
        }, throwable -> Log.d("myLog", "loadNotes: " + throwable.getMessage()));
    }

    public void deleteNote(int id, int position) {
        mDisposable = mDatabase.deleteNote(id).subscribe(() -> {
            mView.updateAdapter(position);
        });
    }
}
