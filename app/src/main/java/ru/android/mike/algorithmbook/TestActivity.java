package ru.android.mike.algorithmbook;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Objects;
import java.util.Queue;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.android.mike.algorithmbook.di.App;
import ru.android.mike.algorithmbook.model.Question;
import ru.android.mike.algorithmbook.mvp.presenter.TestPresenter;
import ru.android.mike.algorithmbook.mvp.view.TestView;

public class TestActivity extends BaseActivity implements TestView {

    @BindViews({R.id.answer_1, R.id.answer_2, R.id.answer_3, R.id.answer_4})
    RadioButton[] mRadioButtons;
    @BindView(R.id.title_test)
    TextView mTitleTest;
    @BindView(R.id.question_title)
    TextView mQuestionTitle;
    @BindView(R.id.radioGroup)
    RadioGroup mRadioGroup;

    @Inject
    TestPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ButterKnife.bind(this);
        App.getComponent().injectTestActivity(this);

        mPresenter.attachView(this);
        mPresenter.loadQuestions();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }

    @Override
    public void updateUi(List<Question> questions, int countAnswer) {
        mRadioGroup.clearCheck();
        String text = "Вопрос " + (countAnswer + 1) + " из " + questions.size();
        mTitleTest.setText(text);
        mQuestionTitle.setText(questions.get(countAnswer).getQuestionTitle());
        for (int i = 0; i < mRadioButtons.length; i++) {
            mRadioButtons[i].setText(questions.get(countAnswer).getAnswers().get(i).getAnswerTitle());
        }
    }

    @Override
    public void showDialog(int correctAnswer, int questionCount) {
        AlertDialog ad = new AlertDialog.Builder(this)
                .setMessage("Правильных ответов " + correctAnswer + " из " + questionCount
                        + " \n Хотите продолжить?")
                .setNegativeButton(R.string.no_btn, (dialog, which) -> finish())
                .setPositiveButton(R.string.ok_btn, (dialog, which) -> mPresenter.resetTest()).create();
        ad.show();
    }

    @OnClick(R.id.check_button)
    public void onViewClicked() {
        for (int i = 0; i < mRadioButtons.length; i++) {
            if (mRadioButtons[i].isChecked()) {
                mPresenter.nextQuestion(i);
            }
        }
    }
}
