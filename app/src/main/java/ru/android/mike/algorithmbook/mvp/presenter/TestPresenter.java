package ru.android.mike.algorithmbook.mvp.presenter;

import android.util.Log;

import java.util.List;

import io.reactivex.disposables.Disposable;
import ru.android.mike.algorithmbook.model.Question;
import ru.android.mike.algorithmbook.mvp.view.TestView;
import ru.android.mike.algorithmbook.strorage.Database;

public class TestPresenter implements Presenter<TestView> {

    private Database mDatabase;
    private TestView mView;

    private int count = 0;
    private int mCorrectAnswer = 0;
    private List<Question> mQuestions;
    private Disposable mDisposable;

    public TestPresenter(Database database) {
        mDatabase = database;
    }

    @Override
    public void attachView(TestView view) {
        mView = view;
    }

    @Override
    public void detachView() {
        count = 0;
        mCorrectAnswer = 0;
        mDisposable.dispose();
        mView = null;
    }

    public void resetTest() {
        mDisposable.dispose();
        count = 0;
        mCorrectAnswer = 0;
        loadQuestions();
    }

    public void nextQuestion(int idAnswer) {
        if (mQuestions.get(count).getAnswers().get(idAnswer).getIsTrue() == 1) {
            mCorrectAnswer++;
        }
        count++;
        if (count == mQuestions.size()) {
            mView.showDialog(mCorrectAnswer, mQuestions.size());
        } else {
            mView.updateUi(mQuestions, count);
        }
    }

    public void loadQuestions() {
        mDisposable = mDatabase.getQuestions("questions")
                .subscribe(questions -> {
                    mQuestions = questions;
                    mView.updateUi(mQuestions, count);
                }, throwable -> Log.d("myLog", throwable.getMessage()));
    }
}
