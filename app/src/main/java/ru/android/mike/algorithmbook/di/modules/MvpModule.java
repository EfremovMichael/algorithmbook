package ru.android.mike.algorithmbook.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import ru.android.mike.algorithmbook.mvp.presenter.NotePresenter;
import ru.android.mike.algorithmbook.mvp.presenter.TestPresenter;
import ru.android.mike.algorithmbook.strorage.Database;
import ru.android.mike.algorithmbook.utils.Firebase;
import ru.android.mike.algorithmbook.mvp.presenter.LoginPresenter;

@Module
public class MvpModule {

    @Provides
    @Singleton
    LoginPresenter providesLoginPresenter(Firebase firebase, CompositeDisposable disposable) {
        return new LoginPresenter(firebase, disposable);
    }

    @Provides
    @Singleton
    TestPresenter provideTestPresenter(Database database){
        return new TestPresenter(database);
    }

    @Provides
    @Singleton
    NotePresenter provideNotePresenter(Database database){
        return new NotePresenter(database);
    }

    @Provides
    @Singleton
    Firebase providesFirebase() {
        return new Firebase();
    }

    @Provides
    @Singleton
    CompositeDisposable provideDisposable() {
        return new CompositeDisposable();
    }

}
