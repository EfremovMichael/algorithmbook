package ru.android.mike.algorithmbook.utils;

import io.reactivex.disposables.CompositeDisposable;

public class RxDisposable {

    private CompositeDisposable mDisposable;

    public RxDisposable() {
        mDisposable = new CompositeDisposable();
    }

    public CompositeDisposable getDisposable() {
        return mDisposable;
    }
}
