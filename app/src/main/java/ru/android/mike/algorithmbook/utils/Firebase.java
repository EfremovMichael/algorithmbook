package ru.android.mike.algorithmbook.utils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import io.reactivex.Single;

public class Firebase {

    private FirebaseAuth mAuth;

    public Firebase() {
        mAuth = FirebaseAuth.getInstance();
    }

    /**
     * Вход по email и паролю
     * @param email Email
     * @param password пароль
     */
    public Single<FirebaseUser> signUser(String email, String password) {
        return Single.create(e ->
                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                e.onSuccess(mAuth.getCurrentUser());
                            } else e.onError(new Exception(task.getException()));
                        }));
    }

    /**
     * Регистрация по email и паролю
     * @param email Email
     * @param password пароль
     */
    public Single<FirebaseUser> createUser(String email, String password) {
        return Single.create(e -> {
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            e.onSuccess(mAuth.getCurrentUser());
                        } else e.onError(new Exception(task.getException()));
                    });
        });
    }

    /**
     * Метод проверяет вошёл ли пользователь в приложение
     */
    public Single<FirebaseUser> checkUser() {
        return Single.create(e -> {
            if (mAuth.getCurrentUser() != null) {
                e.onSuccess(mAuth.getCurrentUser());
            } else e.onError(new Exception("Пожалуйста зарегистрируйтесь"));
        });
    }
}
