package ru.android.mike.algorithmbook.mvp.presenter;

import android.content.Context;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import ru.android.mike.algorithmbook.utils.Firebase;
import ru.android.mike.algorithmbook.mvp.view.LoginView;
import ru.android.mike.algorithmbook.utils.NetworkUtils;

public class LoginPresenter implements Presenter<LoginView> {

    private LoginView mView;
    private Firebase mFirebase;
    private CompositeDisposable mDisposable;

    public LoginPresenter(Firebase firebase, CompositeDisposable disposable) {
        mFirebase = firebase;
        mDisposable = disposable;
    }

    @Override
    public void attachView(LoginView view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mDisposable.clear();
        mView = null;
    }

    public void checkInternet(Context context) {
        if (NetworkUtils.isOnline(context)) {
            mView.setClicked(true);
        } else {
            mView.setClicked(false);
        }
    }

    public void signUser(String email, String password) {
        if (validateForm(email, password)) {
            mView.showProgressBar();
            mDisposable.add(mFirebase.signUser(email, password)
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(user -> {
                        mView.hideProgressBar();
                        mView.goNextActivity(user);
                    }, e -> {
                        mView.hideProgressBar();
                        mView.showToast(e.getMessage());
                    }));
        }
    }

    public void createUser(String email, String password) {
        if (validateForm(email, password)) {
            mView.showProgressBar();
            mDisposable.add(mFirebase.createUser(email, password)
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(user -> {
                        mView.hideProgressBar();
                        mView.goNextActivity(user);
                    }, e -> {
                        mView.hideProgressBar();
                        mView.showToast(e.getMessage());
                    }));
        }
    }

    public void checkUser() {
        mDisposable.add(mFirebase.checkUser()
                .subscribe(user -> mView.goNextActivity(user),
                        throwable -> mView.showToast(throwable.getMessage())));
    }

    private boolean validateForm(String email, String password) {
        if (email.trim().isEmpty() || password.trim().isEmpty()) {
            mView.showToast("Please enter email and password");
            return false;
        }
        return true;
    }
}
