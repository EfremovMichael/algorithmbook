package ru.android.mike.algorithmbook;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.disposables.Disposable;
import ru.android.mike.algorithmbook.di.App;
import ru.android.mike.algorithmbook.strorage.Database;

public class AddNoteActivity extends BaseActivity {

    public static final String NOTE_TITLE = "note_title";
    public static final String NOTE_DESCRIPTION = "note_description";
    public static final String NOTE_ID = "note_id";

    private int flag;

    @BindView(R.id.note_et)
    EditText mNoteEt;
    @BindView(R.id.description_et)
    EditText mDescriptionEt;
    @BindView(R.id.add_btn)
    Button mButton;

    private Database mDatabase;
    private Disposable mDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        ButterKnife.bind(this);

        if (getIntent().getFlags() == 0){
            flag = 0;
            mButton.setText("Перезаписать");
            mNoteEt.setText(getIntent().getStringExtra(NOTE_TITLE));
            mDescriptionEt.setText(getIntent().getStringExtra(NOTE_DESCRIPTION));
        } else {
            flag = 1;
        }

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mDatabase = App.getComponent().getDatabase();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @OnClick(R.id.add_btn)
    public void onViewClicked() {
        if (flag == 1){
            String title = mNoteEt.getText().toString();
            String description = mDescriptionEt.getText().toString();
            if (checkText(title, description)) {
                mDisposable = mDatabase.insertNote(title, description).subscribe();
                finish();
            } else {
                Toast.makeText(this, R.string.error_fill, Toast.LENGTH_SHORT).show();
            }
        } else if (flag == 0){
            String title = mNoteEt.getText().toString();
            String description = mDescriptionEt.getText().toString();
            int id = getIntent().getIntExtra(NOTE_ID, 0);
            mDisposable = mDatabase.editNote(title, description, id).subscribe();
            finish();
        }

    }

    private boolean checkText(String title, String description) {
        return !title.isEmpty() && !description.isEmpty();
    }

    @Override
    protected void onDestroy() {
        if (mDisposable != null){
            mDisposable.dispose();
        }
        super.onDestroy();
    }

}
