package ru.android.mike.algorithmbook.di;

import android.app.Application;

import ru.android.mike.algorithmbook.di.component.AppComponent;
import ru.android.mike.algorithmbook.di.component.DaggerAppComponent;
import ru.android.mike.algorithmbook.di.modules.MvpModule;
import ru.android.mike.algorithmbook.di.modules.StorageModule;

public class App extends Application {

    private static AppComponent sComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        sComponent = DaggerAppComponent.builder().storageModule(new StorageModule(getApplicationContext()))
                .mvpModule(new MvpModule()).build();
    }

    public static AppComponent getComponent() {
        return sComponent;
    }
}
