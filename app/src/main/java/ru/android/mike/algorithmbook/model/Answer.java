package ru.android.mike.algorithmbook.model;

public class Answer {

    private int mId;
    private String mAnswerTitle;
    private int isTrue;

    public Answer(int id, String answerTitle, int isTrue) {
        mId = id;
        mAnswerTitle = answerTitle;
        this.isTrue = isTrue;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getAnswerTitle() {
        return mAnswerTitle;
    }

    public void setAnswerTitle(String answerTitle) {
        mAnswerTitle = answerTitle;
    }

    public int getIsTrue() {
        return isTrue;
    }

}
